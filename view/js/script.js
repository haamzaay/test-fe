// JS code goes here


var sort = 'asc';

function insertContact() {
    var text = document.getElementById('name').value;
    var number = document.getElementById('mobile').value;
    var email = document.getElementById('email').value;

    var error = document.getElementById('error');

    if (text.length >= 20) {
        error.classList.remove('dn');
        error.innerText = 'Must Be less than 20 characters';
        return
    }
    if (!(text.match(/^[a-zA-Z ]*$/))) {
        error.innerText = 'Only Alphabets and spaces are allowed';
        return;
    }

    if (number.length > 10) {
        error.innerText = 'Must Be less than 10 characters';
        return;
    }

    if (email.length >= 40) {
        error.innerText = 'Email must be less than 40 characters';
        return;
    }

    if (!email.match(/^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$/) == null) {
        error.innerText = 'Invalid Email';
        return;
    }

    var o = new Object();
    o.name = text;
    o.mobile = number;
    o.email = email;
    contactsList.push(o);

    bindToTable(false);

}

function bindToTable(isSorted) {

    var TRs = "";
    var list = contactsList;
    if (isSorted) {
        list = sortByKey(contactsList, 'name', sort);
    }

    list.forEach(function (v, k) {
        var style = ``;
        if (k % 2 == 0) style = `style='background-color: #f2f2f2'`;
        TRs += "<tr ></tr><td " + style + ">" + v.name + "</td><td " + style + ">" + v.mobile + "</td>" + "<td " + style + ">" + v.email + "</td></tr>";
    });

    var summaryTable = document.getElementById('summaryTable');
    summaryTable.querySelector('tbody').innerHTML = TRs;
}

function sortByKey(array, key, param) {
    if (param == 'asc') {
        return array.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            sort = 'desc'
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });

    }
    if (param == 'desc') {
        return array.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            sort = 'asc';
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });

    }
}

function sortOnClick() {
    bindToTable(true);
}

